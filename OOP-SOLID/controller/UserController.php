<?php
require 'model/DB_driver.php';
require 'model/User.php';
/**
 * 
 */
require_once 'controller/Controller.php';
class UserController extends Controller
{
	protected $table = 'users';
	function __construct()
  	{
	    $this->folder = 'user';
	   	
  	}
  	public function home(){
  		$db = new DB_driver();
  		$result = $db->table('users')->get();
  		return $this->render('home',$result);
  	}
	public function showCreateForm(){
		if (isset($_GET['id'])) {
			//$record = DB_driver::getItemByID($this->table,$_GET['id']);
			$db = new DB_driver();
			$result = $db->table('users')->where('id',$_GET['id'])->get();
			return $this->render('create-form',$result);
		}else{
			return $this->render('create-form');
		}
	}
	public function create(){
		if (isset($_POST['submit'])) {
			$name = $_POST['name'];
			$email = $_POST['email'];
			$password = $_POST['password'];
			$phone = $_POST['phone'];
			$address = $_POST['address'];
			$field = ['name'=>$name,'email'=>$email,'password'=>$password,'phone'=>$phone,'address'=>$address];
			$db = new DB_driver();
			$result = $db->table('users')->insert($field);
			//DB_driver::addNew($this->table,$field);
		}
	}

	public function update(){
		if (isset($_POST['submit'])) {
			$name = $_POST['name'];
			$email = $_POST['email'];
			$password = $_POST['password'];
			$phone = $_POST['phone'];
			$address = $_POST['address'];
			$field = ['name'=>$name,'email'=>$email,'password'=>$password,'phone'=>$phone,'address'=>$address];
			$db = new DB_driver();
			$result = $db->table('users')->where('id',$_POST['id'])->update($field);
			//DB_driver::updateById($this->table,$_POST['id'],$field);
		}
	}

	public function delete(){
		$db = new DB_driver();
		$result = $db->table('users')->where('id',$_GET['id'])->delete();
		
	}
}