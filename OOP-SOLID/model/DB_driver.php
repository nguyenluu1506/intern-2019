<?php
class DB_driver
{
    public $table = '';
    public $whereoption = '';
    public $order = '';
    public static function connect()
    {
        $db = "mysql:host=localhost;dbname=oopsolid";
        $user = "root";
        $pass = "";
        $conn = new PDO($db,$user,$pass,array(PDO::MYSQL_ATTR_INIT_COMMAND=>"SET NAMES utf8")) or die("Không kết nối được");
        return $conn;
    }
    public function table($table){
        $this->table = $table;
        return $this;
    }
    public function where($field, $value){
        $num_args = func_num_args();
        if($num_args>=3){
            $arg_list = func_get_args();
            
            $this->whereoption=implode("",$arg_list);
        }else{
            $this->whereoption=" $field = $value ";
        }
        return $this;
    }
    public function orderBy($field="id", $order="ASC"){
        $this->order.=($this->order=="")? " $field $order ":" ,$field $order ";
        return $this;
    }

    public function get(){
        $query = self::connect()->prepare("SELECT * FROM $this->table " 
        .($this->whereoption!=''?" Where $this->whereoption ":'')
        .($this->order!=''?" ORDER By $this->order ":'')
        );
        $query->execute();
        return $query->fetchAll();
    }

    public function insert($data){
        if(is_array($data)){
            $field="";
            $val="";
            $prepare = array();
            $i = 0;
            foreach ($data as $key => $value) {
                $i++;
                if($key !="submit"){
                    if($i==1){
                        $field .=$key;
                        $val .=" ? ";
                    }else{
                        $field .= ','.$key;
                        $val .=", ? ";
                    }
                    $prepare[] = $value;
                }
            }
            $sql = "INSERT INTO ".$this->table." ($field) VALUES ($val)";
            $query = self::connect()->prepare($sql);
            $query->execute($prepare);
            unset($prepare);
            header('Location: index.php');
        }
    }
    public function update($data){
        if(is_array($data)){
            $val = "";
            $i = 0;
            $tmp = 1;
            $prepare = array();
            foreach ($data as $key => $value) {
                if($key != "submit"){
                    $i++;
                    if($i == 1){
                        $val .= $key." = ? ";
                    } else{
                        $val .= ", ".$key." = ? ";
                    }
                    $prepare[] = $value;
                }
            }
        }
        $sql = "UPDATE $this->table";
        $sql .= " SET ".$val;
        $sql .= $this->whereoption!='' ? " WHERE $this->whereoption ":'';
        $query = self::connect()->prepare($sql);
        $query->execute($prepare);
        unset($prepare);
        header('Location: index.php');
    }
    public function delete(){
        $sql = "DELETE FROM $this->table ";
        $sql .= $this->whereoption!='' ? " WHERE $this->whereoption ":'';
        $query = self::connect()->prepare($sql);
        $query->execute();
        header('Location: index.php');
    }
	// public static function addNew($table, $data){
	// 	if(is_array($data)){
 //            $field="";
 //            $val="";
 //            $prepare = array();
 //            $i = 0;
 //            foreach ($data as $key => $value) {
 //                $i++;
 //                if($key !="submit"){
 //                    if($i==1){
 //                        $field .=$key;
 //                        $val .=" ? ";
 //                    }else{
 //                        $field .= ','.$key;
 //                        $val .=", ? ";
 //                    }
 //                    $prepare[] = $value;
 //                }
 //            }
 //            $sql = "INSERT INTO ".$table." ($field) VALUES ($val)";
 //            $query = self::connect()->prepare($sql);
 //            $query->execute($prepare);
 //            unset($prepare);
 //            header('Location: index.php');
 //        }
	// }
	// public static function updateById($table,$id,$data){
	// 	if(is_array($data)){
 //            $val = "";
 //            $i = 0;
 //            $tmp = 1;
 //            $prepare = array();
 //            foreach ($data as $key => $value) {
 //                if($key != "submit"){
 //                    $i++;
 //                    if($i == 1){
 //                        $val .= $key." = ? ";
 //                    } else{
 //                        $val .= ", ".$key." = ? ";
 //                    }
 //                    $prepare[] = $value;
 //                }
 //            }
 //        }
 //        $prepare[] = ($id);
 //        $sql = "UPDATE $table";
 //        $sql .= " SET ".$val;
 //        $sql .= " WHERE id = ?";
 //        $query = self::connect()->prepare($sql);
 //        $query->execute($prepare);
 //        unset($prepare);
 //        header('Location: index.php');
	// }
	// public function deleteById($table, $id)
 //    {
 //        $sql = "DELETE FROM ".$table." WHERE id = ?";
 //        $query = self::connect()->prepare($sql);
 //        $query->bindParam(1, $id, PDO::PARAM_INT);
 //        $query->execute();
 //        header('Location: index.php');
 //    }
	// public static function getAll($table){
	// 	$sql = "SELECT * FROM ".$table;
 //        $query = self::connect()->prepare($sql);
 //        $query->execute();
 //        return $query->fetchAll();
	// }
	// public static function getItemByID($table,$id){
	// 	$sql = "SELECT * FROM ".$table." WHERE id = ?";
 //        $query = self::connect()->prepare($sql);
 //        $query->bindParam(1, $id, PDO::PARAM_INT);
 //        $query->execute();
 //        return $query->fetch();
	// }
}
?>