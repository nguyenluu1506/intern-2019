<DOCTYPE html>
<html>
   <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>MVC OOP</title>
  </head>
  <style type="text/css">
    label {
        display: inline-block;
        width: 150px;
    }
    input[type="text"], input[type="password"] {
        display: inline-block;
        width: 200px;
    }
    label.error {
        display: inline-block;
        color:red;
        width: 200px;
    }
    </style>
  <body>
    <?= @$content ?>
  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
  
  <script>
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                name: "required",
                phone: "required",
                email: { 
                    required: true,
                    email : true
                },
                password: "required",
                address: {
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                name: "Vui lòng nhập tên",
                phone: "Vui lòng nhập SĐT",
                email: "Vui lòng nhập email",
                password: "Vui lòng nhập mật khẩu",
                address: {
                    required: "Vui lòng nhập địa chỉ",
                    minlength: "Địa chỉ ngắn vậy, chém gió ah?"
                }
            }
        });
    });
  </script>
</html>