(function($){
    "use strict";
    var name = $('input[name="name"]');
    var email = $('input[name="email"]');
    var message = $('textarea[name="message"]');
    $('.btn').click(function(){
        var check = true;

        if($(name).val().trim() == ''){
            showValidate(name);
            check=false;
        }


        if($(email).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            showValidate(email);
            check=false;
        }

        if($(message).val().trim() == ''){
            showValidate(message);
            check=false;
        }

        return check;
    });

    $('.form-control').each(function(){
        $(this).focus(function(){
           hideValidate(this);
       });
    });
    var showValidate = function(input){
        $(input).parent().children('span').css('display', 'block');
    }

    var hideValidate = function(input){
        $(input).parent().children('span').css('display', 'none');
    }
})(jQuery);

// (function($){
//     "use strict";
//     $('#form').submit(function(){
//         console.log('test');
//         return false;
//     });
        
// })(jQuery);