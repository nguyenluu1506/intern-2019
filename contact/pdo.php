<?php 
	session_start();
	function connect(){
		$db = "mysql:host=localhost;dbname=testpdo";
		$user = "root";
		$pass = "";
		$conn = new PDO($db,$user,$pass,array(PDO::MYSQL_ATTR_INIT_COMMAND=>"SET NAMES utf8")) or die("Không kết nối được");
		return $conn;
	}

	if(isset($_POST['submit'])){
		if(isset($_SESSION['login'])){
			header('Content-Type: text/html; charset=utf-8');
			insert('contact',$_POST);
		}else{
			header("location:login.html");
		}
		
	}
	if (isset($_POST['login'])) {
		login($_POST['email'],$_POST['password']);

	}

	function insert($table,$data){
		if(is_array($data)){
            $field="";
            $val="";
            $prepare = array();
            $i = 0;
            foreach ($data as $key => $value) {
                $i++;
                if($key !="submit"){
                    if($i==1){
                        $field .=$key;
                        $val .=" ? ";
                    }else{
                        $field .= ','.$key;
                        $val .=", ? ";
                    }
                    $prepare[] = $value;
                }
            }
            $sql = "INSERT INTO ".$table." ($field) VALUES ($val)";
            $query = connect()->prepare($sql);
            $query->execute($prepare);
            unset($prepare);
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
	}

	function login($email,$password){

		$sql = "SELECT * FROM user WHERE email = ? and password = ? LIMIT 1";
        $query = connect()->prepare($sql);
        $query->execute([$email, $password]);
        if($query->rowCount() == 1){
  			$_SESSION['login'] = true;
			header("location:index.html");
        }
        else{
        	echo "Đăng nhập thất bại";
        }
	}
?>