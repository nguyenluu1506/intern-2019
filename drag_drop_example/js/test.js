$( function() {
    $("#sortable" ).sortable({
        revert: false,
        receive : function(event, ui){
        	var filename = $(ui.item).attr('value');
        	$.getJSON( "data_json/"+filename+".json", function( data ) {
           		$.each(data,function(index, el) {
    				$(ui.helper[0]).replaceWith(draw(el));
    				$(ui.helper[0]).append(draw(el));
    			});
        	});
        }
    });
    $(".foodgroup_draggable" ).draggable({
        connectToSortable: "#sortable",
        helper: "clone",
        revert: "invalid"
    });
    $(".content_draggable").draggable({
        connectToSortable: "#sortable",
        helper: "clone",
        revert: "invalid"
    });
    $("ul, li" ).disableSelection();
});


var draw = function(data){
	console.log(data);
	var tag = document.createElement(data.tag);
	console.log(tag);
	$(tag).addClass(data.class);
	console.log(data.attr);
	if(data.hasOwnProperty('attr')){
		$.each(data.attr,function(i, j) {
			$(tag).attr(j);
		});
	}
	if (typeof data.content == "string") {
		$(tag).html(data.content);
	}else{
		$.each(data.content,function(index, el) {
			if(typeof el.tag != "img"){
				$(tag).append(draw(el));
			}else{

			}
			
		});
	}
	return tag;
} 