<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minimi - Blogs</title>
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="../css/main.css">
	<style>
		.svg-icon {
		  width: 3em;
		  height: 3em;
		}

		.svg-icon path {
		  fill: #626262;
		}
	</style>
</head>
<body>
	<div class="container">
		<!--header -->
		<?php
			require_once('layouts/header.php');
		?>
		<?php
			if(isset($_GET["view"])){
				$view=$_GET["view"];
				if($_GET["view"]!=''){
					require_once($view.".php");
				}
			}
			else{
				require_once('minimo-1.php');
			}
		?>
		<?php
			require_once('layouts/footer.php');
		?>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function() {
		$("#icon-open").click(function() {
			if($(".menu-collapsible").css("display") == "none"){
				$(".menu-collapsible").css({
					display: 'block'
				});
				$("#icon-open").css({
					transition: '1s',
					transform: 'rotate(-90deg)'
				});
			}else{
				$(".menu-collapsible").css({
					display: 'none'
				});
				$("#icon-open").css({
					transition: '1s',
					transform: 'rotate(-180deg)'
				});
			}
			
		});
	});
</script>
</html>