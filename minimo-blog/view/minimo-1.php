<?php
	require_once('layouts/content-1.php');
?>
<div class="content">
	<div class="content__second">
		<div class="category">
			leave a comment
		</div>
		<div class="article">
			<div class="col-md-6 col-xs-12">
				<div class="img">
					<img src="../images/minimo-1/minimo_07.jpg" width="100%" alt="">
				</div>
				<div class="category_child">
					lifestyle
				</div>
				<div class="title">
					More than just a music festival
				</div>
				<div class="sumary">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="img">
					<img src="../images/minimo-1/minimo_09.png" width="100%" alt="">
				</div>
				<div class="category_child">
					lifestyle
				</div>
				<div class="title">
					Life tastes better with coffee
				</div>
				<div class="sumary">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="img">
					<img src="../images/minimo-1/minimo_13.jpg" width="100%" alt="">
				</div>
				<div class="category_child">
					photodiary
				</div>
				<div class="title">
					American dream
				</div>
				<div class="sumary">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="img">
					<img src="../images/minimo-1/minimo_15.jpg" width="100%" alt="">
				</div>
				<div class="category_child">
					photodiary
				</div>
				<div class="title">
					A day exploring the Alps
				</div>
				<div class="sumary">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
				</div>
			</div>
		</div>
	</div>
</div>
<div class="sign-up">
	<p>Sign up for our newsletter</p>
	<input type="text" class="col-md-4 col-xs-8" placeholder="Enter a valid email address">
	<button><img src="../images/minimo_52.png" alt=""></button>
</div>

<div class="content">
	<div class="content__third">
		<div class="article">
			<div class="col-md-6 col-xs-12">
				<div class="img">
					<img src="../images/minimo-1/minimo_20.jpg" width="100%" alt="">
				</div>
				<div class="category_child">
					lifestyle
				</div>
				<div class="title">
					Top 10 song for running
				</div>
				<div class="sumary">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="img">
					<img src="../images/minimo-1/minimo_21.jpg" width="100%" alt="">
				</div>
				<div class="category_child">
					lifestyle
				</div>
				<div class="title">
					Cold winter day
				</div>
				<div class="sumary">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
				</div>
			</div>
		</div>
		<!-- end arrticle -->
	</div>
	<!--load more -->
	<div class="content__more">
		<button>Load more</button>
	</div>
	<!--end load-more-->
</div>