<?php
	require_once('layouts/content-1.php');
?>
<div class="content">
	<div class="minimo-2">
		<div class="article">
			<img src="../images/minimo-2/minimo_02_03.png" width="100%" alt="">
			<img src="../images/minimo-2/minimo_02_06.png" width="100%" alt="">
			<img src="../images/minimo-2/minimo_02_08.png" width="100%" alt="">
			<blockquote>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum minus, explicabo, beatae quia hic repellat unde velit quidem officia quaerat, eius dicta. Nobis quasi quisquam inventore aperiam odio. Praesentium, dolor quas in ducimus, consequatur expedita accusantium natus esse architecto consectetur reiciendis id tenetur vero officiis, cum vel culpa facere molestiae distinctio, voluptates quaerat quidem provident similique. Officiis quis excepturi deleniti minima laborum molestias fuga, quae magni error! 
			</blockquote>
			<span>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum minus, explicabo, beatae quia hic repellat unde velit quidem officia quaerat, eius dicta. Nobis quasi quisquam inventore aperiam odio. Praesentium, dolor quas in ducimus, consequatur expedita accusantium natus esse architecto consectetur reiciendis id tenetur vero officiis, cum vel culpa facere molestiae distinctio, voluptates quaerat quidem provident similique. Officiis quis excepturi deleniti minima laborum molestias fuga, quae magni error! Debitis libero assumenda eveniet iure pariatur! Ad totam ipsa id, amet impedit sequi deleniti iure autem molestias expedita odio quasi ut reiciendis ducimus corporis aliquid eligendi asperiores porro nemo soluta nulla modi. Nam, tempora minima? Corporis blanditiis omnis iusto reprehenderit quos. Eaque veniam laboriosam, nisi, cumque distinctio numquam? Veritatis neque, quidem eum dolores ipsa aperiam unde, voluptas eligendi quae natus praesentium repudiandae dolorem nostrum, omnis tempora velit cumque minus similique repellat mollitia veniam vel. Autem enim quod perspiciatis esse eligendi facere unde delectus excepturi veritatis dolorum, iste nihil maxime laborum reiciendis ullam sequi beatae quisquam ipsum quos atque praesentium. Officiis possimus nostrum rem, delectus provident aliquam et fugit architecto unde nemo suscipit ipsam eius fuga id officia minima. Officiis molestias iusto amet quo magnam expedita praesentium laborum atque incidunt architecto odio veritatis, sit, accusamus unde rem et voluptas, maiores culpa. 
			</span>

			<div class="share">
			<span>SHARE</span> &nbsp;&nbsp;
		<a href="">
			<svg viewBox="0 0 33 33" width="15" height="15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				<g>
					<path d="M 17.996,32L 12,32 L 12,16 l-4,0 l0-5.514 l 4-0.002l-0.006-3.248C 11.993,2.737, 13.213,0, 18.512,0l 4.412,0 l0,5.515 l-2.757,0 c-2.063,0-2.163,0.77-2.163,2.209l-0.008,2.76l 4.959,0 l-0.585,5.514L 18,16L 17.996,32z"></path>
				</g>
			</svg></a> &nbsp;&nbsp;
		<a href=""><svg viewBox="0 0 33 33" width="15" height="15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M 32,6.076c-1.177,0.522-2.443,0.875-3.771,1.034c 1.355-0.813, 2.396-2.099, 2.887-3.632 c-1.269,0.752-2.674,1.299-4.169,1.593c-1.198-1.276-2.904-2.073-4.792-2.073c-3.626,0-6.565,2.939-6.565,6.565 c0,0.515, 0.058,1.016, 0.17,1.496c-5.456-0.274-10.294-2.888-13.532-6.86c-0.565,0.97-0.889,2.097-0.889,3.301 c0,2.278, 1.159,4.287, 2.921,5.465c-1.076-0.034-2.088-0.329-2.974-0.821c-0.001,0.027-0.001,0.055-0.001,0.083 c0,3.181, 2.263,5.834, 5.266,6.438c-0.551,0.15-1.131,0.23-1.73,0.23c-0.423,0-0.834-0.041-1.235-0.118 c 0.836,2.608, 3.26,4.506, 6.133,4.559c-2.247,1.761-5.078,2.81-8.154,2.81c-0.53,0-1.052-0.031-1.566-0.092 c 2.905,1.863, 6.356,2.95, 10.064,2.95c 12.076,0, 18.679-10.004, 18.679-18.68c0-0.285-0.006-0.568-0.019-0.849 C 30.007,8.548, 31.12,7.392, 32,6.076z"></path></g></svg></a> &nbsp;&nbsp;
		<a href=""><svg viewBox="0 0 33 33" width="15" height="15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M 26.688,0L 5.313,0 C 2.391,0,0,2.391,0,5.313l0,21.375 c0,2.922, 2.391,5.313, 5.313,5.313l 21.375,0 c 2.922,0, 5.313-2.391, 5.313-5.313L 32,5.313 C 32,2.391, 29.609,0, 26.688,0z M 10.244,14l 11.512,0 c 0.218,0.627, 0.338,1.3, 0.338,2c0,3.36-2.734,6.094-6.094,6.094c-3.36,0-6.094-2.734-6.094-6.094 C 9.906,15.3, 10.025,14.627, 10.244,14z M 28,14.002L 28,22 l0,4 c0,1.1-0.9,2-2,2L 6,28 c-1.1,0-2-0.9-2-2l0-4 L 4,14.002 L 4,14 l 3.128,0 c-0.145,0.644-0.222,1.313-0.222,2c0,5.014, 4.079,9.094, 9.094,9.094c 5.014,0, 9.094-4.079, 9.094-9.094 c0-0.687-0.077-1.356-0.222-2L 28,14 L 28,14.002 z M 28,7c0,0.55-0.45,1-1,1l-2,0 c-0.55,0-1-0.45-1-1L 24,5 c0-0.55, 0.45-1, 1-1l 2,0 c 0.55,0, 1,0.45, 1,1L 28,7 z"></path></g></svg></a>
		</div>
		</div>
		
		
		

	</div>
	
</div>
<div class="more">
	<div class="category">
		you may also like
	</div>
	<div class="wrapper">
		<div class="col-md-4 col-xs-12">
			<figure>
				<img src="../images/minimo-2/minimo_02_11.png" width="100%" alt="">
				<figcaption><a href="">Cold winter day</a></figcaption>
			</figure>
		</div>
		<div class="col-md-4 col-xs-12">
			<figure>
				<img src="../images/minimo-2/minimo_02_13.png" width="100%" alt="">
				<figcaption><a href="">A day exploring the Alps</a></figcaption>
			</figure>
		</div>
		<div class="col-md-4 col-xs-12">
			<figure>
				<img src="../images/minimo-2/minimo_02_15.png" width="100%" alt="">
				<figcaption><a href="">American dream</a></figcaption>
			</figure>
		</div>
	</div>
	
</div>

<div class="content">
	<div class="comment">
		<div class="category">
			2 comment
		</div>
		<div class="list-comment">
			<div class="item">
				<div class="item__avt">
					<img src="https://ui-avatars.com/api/?name=John+Doc" alt="">
				</div>
				<div class="item__cmt">
					<div class="name">
						John Doc
					</div>
					<div class="idea">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias porro quam accusantium quod quas praesentium?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias porro quam accusantium quod quas praesentium?Lorem ipsum dolor sit
					</div>
					<div class="reply">
						<span class="category_child -fainter"><a href="">reply</a></span>
					</div>
				</div>
				
			</div>
			<div class="item">
				<div class="item__avt">
					<img src="https://ui-avatars.com/api/?name=Jane+Doe" alt="">
				</div>
				<div class="item__cmt">
					<div class="name">
						Jan Doe
					</div>
					<div class="idea">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, incidunt.
					</div>
					<div class="reply">
						<span class="category_child -fainter"><a href="">reply</a></span>
					</div>
				</div>
				
			</div>
			<div class="item join">
				<div class="item__avt">
					<img src="https://ui-avatars.com/api/?name=Jane+Doe" alt="">
				</div>
				<div class="item__cmt">
					<textarea name="" id="" style="width: 100%;" placeholder="JOIN THE DISCUSSION" rows="2"></textarea>
					<div class="connect">
						<span>Connected with</span>  &nbsp;&nbsp;
		<a href="">
			<svg viewBox="0 0 33 33" width="15" height="15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				<g>
					<path d="M 17.996,32L 12,32 L 12,16 l-4,0 l0-5.514 l 4-0.002l-0.006-3.248C 11.993,2.737, 13.213,0, 18.512,0l 4.412,0 l0,5.515 l-2.757,0 c-2.063,0-2.163,0.77-2.163,2.209l-0.008,2.76l 4.959,0 l-0.585,5.514L 18,16L 17.996,32z"></path>
				</g>
			</svg></a> &nbsp;&nbsp;
		<a href=""><svg viewBox="0 0 33 33" width="15" height="15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M 32,6.076c-1.177,0.522-2.443,0.875-3.771,1.034c 1.355-0.813, 2.396-2.099, 2.887-3.632 c-1.269,0.752-2.674,1.299-4.169,1.593c-1.198-1.276-2.904-2.073-4.792-2.073c-3.626,0-6.565,2.939-6.565,6.565 c0,0.515, 0.058,1.016, 0.17,1.496c-5.456-0.274-10.294-2.888-13.532-6.86c-0.565,0.97-0.889,2.097-0.889,3.301 c0,2.278, 1.159,4.287, 2.921,5.465c-1.076-0.034-2.088-0.329-2.974-0.821c-0.001,0.027-0.001,0.055-0.001,0.083 c0,3.181, 2.263,5.834, 5.266,6.438c-0.551,0.15-1.131,0.23-1.73,0.23c-0.423,0-0.834-0.041-1.235-0.118 c 0.836,2.608, 3.26,4.506, 6.133,4.559c-2.247,1.761-5.078,2.81-8.154,2.81c-0.53,0-1.052-0.031-1.566-0.092 c 2.905,1.863, 6.356,2.95, 10.064,2.95c 12.076,0, 18.679-10.004, 18.679-18.68c0-0.285-0.006-0.568-0.019-0.849 C 30.007,8.548, 31.12,7.392, 32,6.076z"></path></g></svg></a> &nbsp;&nbsp;
		<a href=""><svg viewBox="0 0 33 33" width="15" height="15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M 26.688,0L 5.313,0 C 2.391,0,0,2.391,0,5.313l0,21.375 c0,2.922, 2.391,5.313, 5.313,5.313l 21.375,0 c 2.922,0, 5.313-2.391, 5.313-5.313L 32,5.313 C 32,2.391, 29.609,0, 26.688,0z M 10.244,14l 11.512,0 c 0.218,0.627, 0.338,1.3, 0.338,2c0,3.36-2.734,6.094-6.094,6.094c-3.36,0-6.094-2.734-6.094-6.094 C 9.906,15.3, 10.025,14.627, 10.244,14z M 28,14.002L 28,22 l0,4 c0,1.1-0.9,2-2,2L 6,28 c-1.1,0-2-0.9-2-2l0-4 L 4,14.002 L 4,14 l 3.128,0 c-0.145,0.644-0.222,1.313-0.222,2c0,5.014, 4.079,9.094, 9.094,9.094c 5.014,0, 9.094-4.079, 9.094-9.094 c0-0.687-0.077-1.356-0.222-2L 28,14 L 28,14.002 z M 28,7c0,0.55-0.45,1-1,1l-2,0 c-0.55,0-1-0.45-1-1L 24,5 c0-0.55, 0.45-1, 1-1l 2,0 c 0.55,0, 1,0.45, 1,1L 28,7 z"></path></g></svg></a>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
</div>