window.onload = function() {
    var btnPrev = document.getElementsByClassName("btn__prev")[0];
    var btnNext = document.getElementsByClassName("btn__next")[0];
    btnNext.addEventListener("click", function() {
        next();
    });
    btnPrev.addEventListener("click", function() {
        previous();
    });
}
var i =0;
function go(j){
    i = j-1;
    disableActiveDotted();
    document.getElementsByClassName("btn__prev")[0].classList.remove("btn--disable");
    document.getElementsByClassName("btn__next")[0].classList.remove("btn--disable");
    document.getElementById("dotted"+(j)).classList.add("slide-active");
    document.getElementById('mirror').style.transform = 'translateX(-' + (568*(j-1)) + 'px)';
    if(j == 1){
        document.getElementsByClassName("btn__prev")[0].classList.add("btn--disable");
    }
    if(j == 5){
        document.getElementsByClassName("btn__next")[0].classList.add("btn--disable");
    } 
}
function disableActiveDotted(){
    document.getElementById("dotted1").classList.remove("slide-active");
    document.getElementById("dotted2").classList.remove("slide-active");
    document.getElementById("dotted3").classList.remove("slide-active");
    document.getElementById("dotted4").classList.remove("slide-active");
    document.getElementById("dotted5").classList.remove("slide-active");
}
function next() {
	i++;
    disableActiveDotted();
    document.getElementById("dotted"+(i+1)).classList.add("slide-active");
    if (i >= 4) {
        document.getElementById('mirror').style.transform = 'translateX(-' + (540*i) + 'px)';
        document.getElementsByClassName("btn__next")[0].classList.add("btn--disable");
        i=4;
    }
    else{
        document.getElementById('mirror').style.transform = 'translateX(-' + (568*i) + 'px)';
        document.getElementsByClassName("btn__prev")[0].classList.remove("btn--disable");
    }
}
function previous() {
    i--;
    disableActiveDotted();
    document.getElementById("dotted"+(i+1)).classList.add("slide-active");
    if (i<=0) {
        document.getElementById('mirror').style.transform = 'translateX(0px)';
        document.getElementsByClassName("btn__prev")[0].classList.add("btn--disable");
        i=0;
    }
    else{
        document.getElementById('mirror').style.transform = 'translateX(-' + (568*i) + 'px)';
        document.getElementsByClassName("btn__next")[0].classList.remove("btn--disable");
    }
}