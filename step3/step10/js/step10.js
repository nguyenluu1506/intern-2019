window.onload = function() {
    var btnPrev = document.getElementsByClassName("btn__prev")[0];
    var btnNext = document.getElementsByClassName("btn__next")[0];
    btnNext.addEventListener("click", function() {
        next();
    });
    btnPrev.addEventListener("click", function() {
        previous();
    });
}
var i =0;
function go(j){
    i = j-1;
    disableActiveDotted();
    document.getElementsByClassName("btn__prev")[0].classList.remove("btn--disable");
    document.getElementsByClassName("btn__next")[0].classList.remove("btn--disable");
    document.getElementById("dotted"+(j)).classList.add("slide-active");
    document.getElementById('mirror').style.transform = 'translateX(-' + (568*(j-1)) + 'px)';
    if(j == 1){
        document.getElementsByClassName("btn__prev")[0].classList.add("btn--disable");
    }
    if(j == 4){
        document.getElementsByClassName("btn__next")[0].classList.add("btn--disable");
    } 
}
function disableActiveDotted(){
    document.getElementById("dotted1").classList.remove("slide-active");
    document.getElementById("dotted2").classList.remove("slide-active");
    document.getElementById("dotted3").classList.remove("slide-active");
    document.getElementById("dotted4").classList.remove("slide-active");
}
function next() {
	i++;
    disableActiveDotted();
    document.getElementById("dotted"+(i+1)).classList.add("slide-active");
    if (i >= 3) {
        document.getElementById('mirror').style.transform = 'translateX(-' + (520*i) + 'px)';
        document.getElementsByClassName("btn__next")[0].classList.add("btn--disable");
        i=3;
    }
    else{
        document.getElementById('mirror').style.transform = 'translateX(-' + (568*i) + 'px)';
        document.getElementsByClassName("btn__prev")[0].classList.remove("btn--disable");
    }
}
function previous() {
    i--;
    disableActiveDotted();
    document.getElementById("dotted"+(i+1)).classList.add("slide-active");
    if (i<=0) {
        document.getElementById('mirror').style.transform = 'translateX(0px)';
        document.getElementsByClassName("btn__prev")[0].classList.add("btn--disable");
        i=0;
    }
    else{
        document.getElementById('mirror').style.transform = 'translateX(-' + (568*i) + 'px)';
        document.getElementsByClassName("btn__next")[0].classList.remove("btn--disable");
    }
}