// window.onload = function() {
//     var btnPrev = document.getElementsByClassName("btn__prev")[0];
//     var btnNext = document.getElementsByClassName("btn__next")[0];
//     btnNext.addEventListener("click", function() {
//         next();
//     });
//     btnPrev.addEventListener("click", function() {
//         previous();
//     });
// }
// var i =0;
// function go(j){
//     i = j-1;
//     disableActiveDotted();
//     document.getElementsByClassName("btn__prev")[0].classList.remove("btn--disable");
//     document.getElementsByClassName("btn__next")[0].classList.remove("btn--disable");
//     document.getElementById("dotted"+(j)).classList.add("slide-active");
//     document.getElementById('mirror').style.transform = 'translateX(-' + (568*(j-1)) + 'px)';
//     if(j == 1){
//         document.getElementsByClassName("btn__prev")[0].classList.add("btn--disable");
//     }
//     if(j == 5){
//         document.getElementsByClassName("btn__next")[0].classList.add("btn--disable");
//     } 
// }
// function disableActiveDotted(){
//     document.getElementById("dotted1").classList.remove("slide-active");
//     document.getElementById("dotted2").classList.remove("slide-active");
//     document.getElementById("dotted3").classList.remove("slide-active");
//     document.getElementById("dotted4").classList.remove("slide-active");
//     document.getElementById("dotted5").classList.remove("slide-active");
// }
// function next() {
// 	i++;
//     disableActiveDotted();
//     document.getElementById("dotted"+(i+1)).classList.add("slide-active");
//     if (i >= 4) {
//         document.getElementById('mirror').style.transform = 'translateX(-' + (540*i) + 'px)';
//         document.getElementsByClassName("btn__next")[0].classList.add("btn--disable");
//         i=4;
//     }
//     else{
//         document.getElementById('mirror').style.transform = 'translateX(-' + (568*i) + 'px)';
//         document.getElementsByClassName("btn__prev")[0].classList.remove("btn--disable");
//     }
// }
// function previous() {
//     i--;
//     disableActiveDotted();
//     document.getElementById("dotted"+(i+1)).classList.add("slide-active");
//     if (i<=0) {
//         document.getElementById('mirror').style.transform = 'translateX(0px)';
//         document.getElementsByClassName("btn__prev")[0].classList.add("btn--disable");
//         i=0;
//     }
//     else{
//         document.getElementById('mirror').style.transform = 'translateX(-' + (568*i) + 'px)';
//         document.getElementsByClassName("btn__next")[0].classList.remove("btn--disable");
//     }
// }

(function($){
    $.fn.gallery=function(option){
        var option=option ||{step:2}
        var that=this;
        var len=Math.ceil($(that).find(".gallery__item").length/option.step);
        that.find('.control').html('<span class="dotted"></span>'.repeat(len));
        if(len>0){
            that.find('.control .dotted').first().addClass('slide-active');
        }
        var i = 0;
        that.find(".btn__next").click(function() {
                i++;
                that.find(".slide-active").removeClass("slide-active");
                that.find(".dotted").eq(i).addClass('slide-active');
                if (i >= 4) {
                    that.find('.mirror').css('transform', 'translateX(-' + (540*i) + 'px)');
                    that.find(".btn__next").addClass("btn--disable");
                    i=4;
                }
                else{
                    that.find('.mirror').css('transform', 'translateX(-' + (568*i) + 'px)');
                    that.find(".btn__prev").removeClass("btn--disable");
                }
            });
        
        that.find(".btn__prev").click(function() {
                i--;
                that.find(".slide-active").removeClass("slide-active");
                that.find(".dotted").eq(i).addClass('slide-active');
                if (i<=0) {
                    that.find('.mirror').css('transform', 'translateX(0px)');
                    that.find(".btn__prev").addClass("btn--disable");
                    i=0;
                }
                else{
                    that.find('.mirror').css('transform', 'translateX(-' + (568*i) + 'px)');
                    that.find(".btn__next").removeClass("btn--disable");
                }

            });
        
      
        that.find(".control .dotted").click(function() {
            that.find('.slide-active').removeClass("slide-active");
            that.find(".btn__prev").removeClass("btn--disable");
            that.find(".btn__next").removeClass("btn--disable");
            that.find(".mirror").css('transform', 'translateX(-' + (568*($(this).index())) + 'px)');
            $(this).addClass('slide-active');
            if($(this).index() == 4){
                that.find(".btn__next").addClass("btn--disable");
            } 
            if($(this).index() == 0){
                that.find(".btn__prev").addClass("btn--disable");
            }
        });
                
                        
        
        return this;
    }
   
})(jQuery);
$(document).ready(function() {
    $('.news').gallery();
    $('.news1').gallery();
});



