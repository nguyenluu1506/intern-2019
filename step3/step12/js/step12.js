var i=0;
window.onload = function() {
	var up = document.getElementById("up");
	var down = document.getElementById("down");
    var totalPage = document.getElementById("box").children.length;
    document.getElementById("page").innerHTML = i+1+"/"+totalPage;
	up.addEventListener("click", function() {
		clickUp();
		document.getElementById("page").innerHTML = i+1+"/"+totalPage;
	});
	down.addEventListener("click", function() {
		clickDown();
		document.getElementById("page").innerHTML = i+1+"/"+totalPage;
	})
	
}

function clickUp() {
	i--;
	document.getElementById("box").style.transform = 'translatey(-'+(165*i)+'px)';
	if(i<0){
		i=0;
	}
}

function clickDown(){
	i++;
	if(i>3){
		document.getElementById("box").style.transform = 'translatey(-'+(165*(i-1))+'px)';
		i=3;
	}else{
		document.getElementById("box").style.transform = 'translatey(-'+(165*i)+'px)';
	}
	
}
